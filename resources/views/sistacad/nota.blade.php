@extends('layouts.master')

@section('title', 'Professor')


@section('content')


<!-- BEGIN PAGE CONTENT -->
<div class="page-content">
	<div class="container">
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<a href="#">Home</a><i class="fa fa-circle"></i>
			</li>
			<li class="active">
				 Notas do Bimestre
			</li>
		</ul>
		
<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet light">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift font-green-sharp"></i>
								<span class="caption-subject font-green-sharp bold uppercase">Lançamento </span>
								<span class="caption-helper">de Notas...</span>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-container">
								
								<table class="table table-striped table-bordered table-hover" id="datatable_products">
								<thead>
								<tr role="row" class="heading">
									<th width="10%">
										 ID
									</th>
									<th width="15%">
										 Nome do Aluno
									</th>
									<th width="15%">
										 Chamada
									</th>
									<th width="10%">
										 Bimestre
									</th>
									<th width="10%">
										 Nota
									</th>
									<th width="10%">
										 Matéria
									</th>
									<th width="10%"></th>
								</tr>
								<tr role="row" class="filter">
									<td>
										<input type="text" class="form-control form-filter input-sm" readonly id="id_aluno" name="id_aluno">
									</td>
									<td>
										<input type="text" id="nome_aluno" class="form-control form-filter input-sm" name="nome_aluno">
									</td>
									<td>
										<select name="chamada_prova" class="form-control form-filter input-sm">
											<option value="">...</option>
											<option value="1">Primeira</option>
											<option value="2">Segunda</option>
											<option value="3">Terceira</option>
										</select>
									</td>
									<td>
										<select name="bimestre" class="form-control form-filter input-sm">
											<option value="">...</option>
											<option value="1">Primeiro</option>
											<option value="2">Segundo</option>
											<option value="3">Terceiro</option>
											<option value="3">Quarto</option>
										</select>
									</td>
									<td>
										<div class="margin-bottom-5">
											<input type="text" class="form-control form-filter input-sm" id="nota" name="nota" placeholder="Nota do Aluno" />
										</div>
									</td>
									<td>
										<div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
											<input type="text" class="form-control form-filter input-sm" name="materia" placeholder="Matéria">
											
										</div>
									</td>
									
									<td>
										<button class="btn btn-sm green filter-submit margin-bottom"><i class="fa fa-send"></i> Enviar</button>
									</td>
								</tr>
								</thead>
								<tbody>
								</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>



		<h1>Formulario de lancamento de notas </h1>
	</div>
</div>
<!-- END PAGE CONTENT -->
@endsection