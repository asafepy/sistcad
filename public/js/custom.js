/**
 * Created by nobre on 01/06/17.
 */
jQuery(function($) {
    $.mask.definitions['~']='[+-]';
    //Inicio Mascara Telefone
    $('input[type=tel]').focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if(phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');
    //Fim Mascara Telefone
    $(".ncm").mask("9999.99.99");
    $(".cest").mask("99.999.99");
    $(".parcela").mask("99/99");
});
