<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Professor;

class ProfessorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
    	DB::table('professor')->truncate();
    	$faker = Faker::create();
    	
    	foreach(range(1,5) as $i)
    	{
    		Professor::create([
    			'nome' => $faker->word()
    		]);
    	}

    }
}
