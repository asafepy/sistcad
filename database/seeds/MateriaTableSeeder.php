<?php

use Illuminate\Database\Seeder;
use App\Materia;
use Faker\Factory as Faker;

class MateriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	foreach(range(1,6) as $i)
    	{

    		DB::table('materia')->truncate();
    		$faker = Faker::create();

    		Materia::create([
    			'nome' => $faker->word()
    		]);
    	}
        
    }
}
