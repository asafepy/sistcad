<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Aluno;
class AlunoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('aluno')->truncate();
        $faker = Faker::create();

        foreach (range(1, 30) as $i) {
        	 Aluno::create([
	        	'nome' => $faker->name()
	        	]);
        }
       
    }
}
