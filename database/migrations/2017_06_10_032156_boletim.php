<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Boletim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boletim', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->float('nota');

            $table->string('bimestre');
            $table->integer('id_materia')->unsigned();
            $table->integer('id_professor')->unsigned();
            $table->integer('id_aluno')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('boletim');
    }
}
