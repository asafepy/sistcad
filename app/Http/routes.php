<?php

Route::get('/', function () {
    return redirect('painel');
});






Route::group(['prefix' => 'painel', 'middleware' => 'auth'], function(){
	
		Route::get('/', ['as' => 'prof.index', 'uses' => 'SistCadController@index']);
		Route::get('nota', ['as' => 'prof.nota', 'uses' => 'SistCadController@nota']);
		Route::get('ad', ['as' => 'prof.ad', 'uses' => 'SistCadController@ad']);
		Route::get('noticia', ['as' => 'prof.noticia', 'uses' => 'SistCadController@noticia']);
});


Route::get('autocomplete', function()
{
return View::make('autocomplete');
});




Route::auth();

Route::get('/home', 'HomeController@index');


