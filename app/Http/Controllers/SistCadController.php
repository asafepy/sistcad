<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SistCadController extends Controller
{
    public function index()
    {
    	return view('sistacad.index');
    }

    public function nota()
    {

    	return view('sistacad.nota');
    }

    
    public function autocomplete(Request $request)
    {

      $term=$request->term;
      $data=PaymentInvoice::where('invoice_number','LIKE','%'.$term.'%')->take(10)->get();
      //var_dump($data);

      $results=array();


      foreach ($data as $key => $v) {

          $results[]=['id'=>$v->id,'value'=>$v->invoice_number." Project Name: ".$v->project_name." Amount: ".$v->amount];

      }

      return response()->json($results);

    }



    public function ad()
    {
    	return view('sistacad.ad');
    }


    public function noticia()
    {
    	return view('sistacad.noticia');
    }
}
