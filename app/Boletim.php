<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Boletim extends Model
{
    protected $table = 'boletim';
   	protected $fillable = [
   		'nome', 
   		'nota', 
   		'bimestre',
   		'materia_id',
   		'professor_id',
   		'aluno_id',
   	];
}
